# Génération / vérification de TOTP

## Ressources 

 * Explication un peu complète, trop complexe pour notre exemple mais instructive : https://www.ionos.fr/digitalguide/serveur/securite/totp/
 * Tuto rapide sur spring boot : https://www.baeldung.com/spring-boot-console-app
 * Doc de Mockito : https://site.mockito.org/#how
 * Tester avec des dates : https://www.baeldung.com/java-override-system-time

## Algorithme de base

### Enregistrement d'un utilisateur

Un utilisateur s'enregistre avec son email (qui sera son identifiant)

1. L'utilisateur fournit son email
2. Le système génère une chaine aléatoire (une «clé secrète» qui sera partagée entre l'utilisateur et le système)
3. Le système crée et stocke un objet qui associe l'utilisateur à cette clé secrète
4. Le système donne la clé secrète à l'utilisateur (c'est cette partie qui est souvent faite avec un QR code !)

### Génération du code

Cette partie est faîte soit par le client pour généréer le code (avec son smartphone : Google Authenticator) soit par le serveur pour vérifier le code.

1. On détermine la « période » actuelle : division entière du timestamp par la durée de validité voulue d'un code (souvent 30s)
2. On combine la clé secrète avec la période : pour notre exemple simplifié (et non sécurisé) ça sera une addition.
3. On « hache » le résultat de cette combinaison : pour notre exemple simplifié (attention, il n'est pas sécurisé ;) ) ça sera un modulo

### Vérification du login

1. L'utilisateur soumet son identifiant (email) et le code qu'il a généré de son côté
2. Le système récupère l'utilisateur grace à son identifiant puis récupère la clé secrète de cet utilisateur
3. à l'aide de la clé secrète, le système génère le code actuel
4. Si le code actuel généré par le système correspond au code fourni, le login est un succès ! Sinon, c'est un échec :( 


## Modélisation

### Enregistrement d'un utilisateur

```mermaid
sequenceDiagram
    participant M as Main
    participant RS as RegisterService
    participant RKG as RandomKeyGenerator
    participant UR as UserRepository
    M->>RS: register(email)
    activate RS
    RS->>RKG: generateKey()
    RKG->>RS: 
    RS->>RS: create User instance
    RS->>UR: save(user)
    RS->>M: return secret key
    deactivate RS
```

### Génération d'un code

```mermaid
sequenceDiagram
    participant M as Main
    participant TG as TotpGenerator
    M->>TG: generate(secret)
    activate TG
    TG->>TG: get current timestamp
    TG->>TG: combine and hash timestamp and secret
    TG->>M: return current code
    deactivate TG
```

### Processus de login

```mermaid
sequenceDiagram
    participant M as Main
    participant LS as LoginService
    participant UR as UserRepository
    participant TG as TotpGenerator
    M->>LS: attemptLogin(email, code)
    activate LS
    LS->>UR: find user by email
    UR->>LS: 
    LS->>TG: generate(user secret)
    TG->>LS: 
    LS->>LS: compare given code and generated code 
    LS->>M: OK/KO
    deactivate LS
```