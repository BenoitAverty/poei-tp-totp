package com.zenika.academy.totp.service;

import com.zenika.academy.totp.model.User;
import com.zenika.academy.totp.storage.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class LoginServiceTest {
    
    @Test
    @DisplayName("Login success case")
    void loginSuccess() {
        // Given
        UserRepository userRpository = Mockito.mock(UserRepository.class);
        TotpGenerator totpGenerator = Mockito.mock(TotpGenerator.class);
        LoginService loginService = new LoginService(userRpository, totpGenerator);
        User fakeUser = new User("academy@zenika.com", "secret");
        
        // When
        Mockito.when(userRpository.findByEmail("academy@zenika.com")).thenReturn(Optional.of(fakeUser));
        Mockito.when(totpGenerator.generate("secret")).thenReturn("123456");
        
        boolean result = loginService.attemptLogin("academy@zenika.com", "123456");
        
        // Then
        assertTrue(result);
    }

    @Test
    @DisplayName("Bad password case")
    void wrongCode() {
        // Given
        UserRepository userRpository = Mockito.mock(UserRepository.class);
        TotpGenerator totpGenerator = Mockito.mock(TotpGenerator.class);
        LoginService loginService = new LoginService(userRpository, totpGenerator);
        User fakeUser = new User("academy@zenika.com", "secret");

        // When
        Mockito.when(userRpository.findByEmail("academy@zenika.com")).thenReturn(Optional.of(fakeUser));
        Mockito.when(totpGenerator.generate("secret")).thenReturn("123456");

        boolean result = loginService.attemptLogin("academy@zenika.com", "222222");

        // Then
        assertFalse(result);
    }

    @Test
    @DisplayName("Wrong email case")
    void nonExistingUser() {
        // Given
        UserRepository userRpository = Mockito.mock(UserRepository.class);
        TotpGenerator totpGenerator = Mockito.mock(TotpGenerator.class);
        LoginService loginService = new LoginService(userRpository, totpGenerator);

        // When
        Mockito.when(userRpository.findByEmail("academy@zenika.com")).thenReturn(Optional.empty());

        boolean result = loginService.attemptLogin("academy@zenika.com", "123456");

        // Then
        assertFalse(result);
        verify(totpGenerator, never()).generate(anyString());
    }

}