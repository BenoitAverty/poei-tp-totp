package com.zenika.academy;

import com.zenika.academy.totp.model.User;
import com.zenika.academy.totp.service.LoginService;
import com.zenika.academy.totp.service.RegisterService;
import com.zenika.academy.totp.service.SecretKeyGenerator;
import com.zenika.academy.totp.service.TotpGenerator;
import com.zenika.academy.totp.storage.UserRepository;

import java.time.Clock;
import java.util.Random;
import java.util.Scanner;
import java.util.function.BiFunction;

public class App {
    public static void main(String[] args) {
        // créer les objets dont on pourrait avoir besoin...
        final BiFunction<String, String, String> hashFunction = (a, b) -> String.valueOf(Math.abs(a.concat(new StringBuffer(b).reverse().toString()).hashCode()) % 1000000);
        SecretKeyGenerator secretKeyGenerator = new SecretKeyGenerator(new Random());
        UserRepository userRepository = new UserRepository();
        RegisterService registerService = new RegisterService(secretKeyGenerator, userRepository);
        TotpGenerator totpGenerator = new TotpGenerator(Clock.systemDefaultZone(), hashFunction, 60);
        LoginService loginService = new LoginService(userRepository, totpGenerator);


        while (true) {

            final String[] action;
            System.out.println("Tapez l'action à effectuer");
            final String input = new Scanner(System.in).nextLine();
            action = input.split(" ");
            if (action.length == 0) continue;

            switch (action[0]) {
                case "register":
                    String newUserKey = registerService.register(action[1]);
                    System.out.println("Secret key : " + newUserKey + ". Store it in a safe place.");
                    break;
                case "generate":
                    String code = totpGenerator.generate(action[1]);
                    System.out.println("Le code actuel est " + code);
                    break;
                case "login":
                    final boolean loginSuccess = loginService.attemptLogin(action[1], action[2]);
                    System.out.println(loginSuccess ? "Ok, you're in !" : "Sorry, that's not right...");
                    break;
                case "stop":
                    System.exit(0);
                default:
                    System.out.println("Erreur : action inconnue");
                    break;
            }
        }
    }
}
