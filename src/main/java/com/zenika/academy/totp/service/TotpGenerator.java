package com.zenika.academy.totp.service;

import java.time.Clock;
import java.util.function.BiFunction;

public class TotpGenerator {
    /**
     * Clock permettant à ce service de savoir l'heure qu'il est
     */
    private final Clock clock;

    /**
     * Fonction de hachage pour l'heure et le secret.
     */
    private final BiFunction<String, String, String> hashFunction;

    /**
     * Durée de la période de validité du Totp
     */
    private final int period;

    public TotpGenerator(Clock clock, BiFunction<String, String, String> hashFunction, int period) {
        this.clock = clock;
        this.hashFunction = hashFunction;
        this.period = period;
    }

    /**
     * Génère le code actuel pour une clé secrète donnée
     */
    public String generate(String secretKey) {
        final long epochSecond = clock.millis() / 1000;
        
        final long currentPeriod = Math.floorDiv(epochSecond, period);
        return hashFunction.apply(secretKey, String.valueOf(currentPeriod));
    }
}
