package com.zenika.academy.totp.service;

import com.zenika.academy.totp.model.User;
import com.zenika.academy.totp.storage.UserRepository;

public class RegisterService {
    private final SecretKeyGenerator secretKeyGenerator;
    private final UserRepository userRepository;

    public RegisterService(SecretKeyGenerator secretKeyGenerator, UserRepository userRepository) {
        this.secretKeyGenerator = secretKeyGenerator;
        this.userRepository = userRepository;
    }

    /**
     * Register a user and return its secret key
     * @param email
     * @return
     */
    public String register(String email) {
        User newUser = new User(email, secretKeyGenerator.generate());
        
        userRepository.save(newUser);
        
        return newUser.getSecretKey();
    }
}
